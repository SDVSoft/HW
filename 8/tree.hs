import Prelude hiding (lookup)

-- Implement a binary search tree (4 points)
-- 2 extra points for a balanced tree
data BinaryTree k v = Nil | Node k v (BinaryTree k v) (BinaryTree k v)

-- �Ord k =>� requires, that the elements of type k are comparable
-- Takes a key and a tree and returns Just value if the given key is present,
-- otherwise returns Nothing
lookup :: Ord k => k -> (BinaryTree k v) -> Maybe v
lookup _ Nil = Nothing
lookup key (Node k v l r)
     | key == k  = Just v
     | key > k   = lookup key r
     | otherwise = lookup key l

-- Takes a key, value and tree and returns a new tree with key/value pair inserted.
-- If the given key was already present, the value is updated in the new tree.
insert :: Ord k => k -> v -> (BinaryTree k v) -> (BinaryTree k v)
insert key value Nil = (Node key value Nil Nil)
insert key value (Node k v l r)
     | key == k  = (Node key value l r)
     | key > k   = (Node k v l (insert key value r))
     | otherwise = (Node k v (insert key value l) r)

getGreatest (Node k v _ Nil)   = (k, v)
getGreatest (Node _ _ _ right) = getGreatest right

deleteGreatest Nil = Nil
deleteGreatest (Node _ _ l Nil) = l
deleteGreatest (Node k v l r)   = (Node k v l (deleteGreatest r))

merge Nil r = r
merge l r   = let (k', v') = (getGreatest l) in
              Node k' v' (deleteGreatest l) r

-- Returns a new tree without the given key
delete :: Ord k => k -> (BinaryTree k v) -> (BinaryTree k v)
delete _ Nil = Nil
delete key (Node k v l r)
     | key == k  = merge l r
     | key > k   = (Node k v l (delete key r))
     | otherwise = (Node k v (delete key l) r)