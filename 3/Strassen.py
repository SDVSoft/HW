import numpy as np


def read_matrix(n):
    a = np.empty((n, n))
    for i in range(n):
        a[i] = input().split()
    return a


def print_matrix(a):
    for row in a:
        print(' '.join(map(str, row)))


def divide_into_4(a):
    a1, a2 = np.vsplit(a, 2)
    a11, a12 = np.hsplit(a1, 2)
    a21, a22 = np.hsplit(a2, 2)
    return a11, a12, a21, a22


def join_4(a11, a12, a21, a22):
    a1 = np.hstack((a11, a12))
    a2 = np.hstack((a21, a22))
    return np.vstack((a1, a2))


def strassen(a, b):
    if a.shape == (1, 1):
        return a * b
    a11, a12, a21, a22 = divide_into_4(a)
    b11, b12, b21, b22 = divide_into_4(b)

    m1 = strassen(a11 + a22, b11 + b22)
    m2 = strassen(a21 + a22, b11)
    m3 = strassen(a11, b12 - b22)
    m4 = strassen(a22, b21 - b11)
    m5 = strassen(a11 + a12, b22)
    m6 = strassen(a21 - a11, b11 + b12)
    m7 = strassen(a12 - a22, b21 + b22)

    c11 = m1 + m4 + m7 - m5
    c12 = m3 + m5
    c21 = m2 + m4
    c22 = m1 - m2 + m3 + m6

    return join_4(c11, c12, c21, c22)


def main():
    n = int(input())
    size = 1
    while size < n:
        size *= 2
    a = np.zeros((size, size), dtype=np.int64)
    b = np.zeros((size, size), dtype=np.int64)
    a[:n, :n] = read_matrix(n)
    b[:n, :n] = read_matrix(n)
    c = strassen(a, b)
    print_matrix(c[:n, :n])


if __name__ == '__main__':
    main()
