import os
from hashlib import sha1 as hasher
import sys
import timeit


def file_dhash(f):
    h = hasher()
    f_length = os.stat(f.name).st_size
    while f.tell() != f_length:
        h.update(f.read(262144))
    return h.digest()

def main():
    if len(sys.argv) != 2:
        print ("usage: ./duplicate_files dir.name")
        sys.exit(1)
        
    top = sys.argv[1]
    f_hash = {}
    for path, _, files in os.walk(top):
        for f_name in files:
            f_pname = os.path.join(path, f_name)
            if f_name[0] != '.' and f_name[0] != '~' and not os.path.islink(f_pname):
                with open(f_pname, mode = 'rb') as f:
                    h = file_dhash(f)
                    if h not in f_hash:
                        f_hash[h] = [f_pname]
                    else:
                        f_hash[h] += [f_pname]
    for copies in f_hash.values():
        if len(copies) > 1:
            print(':'.join(copies))
        
if __name__ == "__main__":
    main()
