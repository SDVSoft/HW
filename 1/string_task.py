# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
#
# Example input: 'read'
# Example output: 'reading'
def verbing(s):
    if len(s) < 3:
        return s
    elif s.endswith("ing"):
        return s + "ly"
    else: 
        return s + "ing"


# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
#
# Example input: 'This dinner is not that bad!'
# Example output: 'This dinner is good!'
def not_bad(s):
    noti = s.find("not")
    badi = s.find("bad")
    if noti != -1 and noti < badi:
        return s.replace(s[noti: badi + 3], "good")
    return s


# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
#
# Given 2 strings, a and b, return a string of the form
#  a-front + b-front + a-back + b-back
#
# Example input: 'abcd', 'xy'
# Example output: 'abxcdy'
def bisect(s, half):
    mid = (len(s) + 1)/ 2
    return s[:mid] if half == 1 else s[mid:]
    
def front_back(a, b):
    return bisect(a, 1) + bisect(b, 1) + bisect(a, 2) + bisect(b, 2)

