
import sys

def count_words(words):
    freq = {}
    for word in words:
        word = word.lower()
        freq[word] = freq.setdefault(word, 0) + 1
    return freq

def print_words(filename):
    words = read_words(filename)
    freq = count_words(words)
    for word, f in sorted(freq.items()):
        print word, f

def print_top(filename):
    words = read_words(filename)
    freq = count_words(words)
    
    for word, f in sorted(freq.items(), key = lambda f: f[1], reverse = True)[:20]:
        print word, f

def read_words(filename):
    words = []
    with open(filename, "r") as f:
        for line in f:
            words.extend(line.split())
    return words

def main():
    if len(sys.argv) != 3:
        print('usage: ./wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print('unknown option: ' + option)
        sys.exit(1)

if __name__ == '__main__':
    main()
