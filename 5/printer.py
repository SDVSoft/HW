from yat.model import Scope, Number, ExpressionList, Function, FunctionCall
from yat.model import FunctionDefinition, Conditional, Print, Read, Reference
from yat.model import BinaryOperation, UnaryOperation


class PrettyPrinter:
    def __init__(self):
        self.indent = 0
        self.outer_operation_rating = 0

    def visit(self, tree):
        tree.accept(self)
        print(";")

    def visitNumber(self, num):
        parentheses = False
        if num.value < 0 and self.outer_operation_rating > 2:
            print("(", end="")
            parentheses = True
        print(num.value, end="")
        if parentheses:
            print(")", end="")

    def visitExpressionList(self, expr_list):
        expr_list = expr_list.expr_list
        for expr in expr_list:
            print("    " * self.indent, end="")
            expr.accept(self)
            print(";")

    def visitFunctionDefinition(self, f_def):
        function = f_def.function
        print("def " + f_def.name + "(" + ", ".join(function.args) + ") {")
        self.indent += 1
        function.body.accept(self)
        self.indent -= 1
        print("    " * self.indent + "}", end="")

    def visitConditional(self, conditional):
        print("if (", end="")
        conditional.condition.accept(self)
        print(") {")
        self.indent += 1
        conditional.if_true.accept(self)
        self.indent -= 1
        if conditional.if_false.expr_list != []:
            print("    " * self.indent + "} else {")
            self.indent += 1
            conditional.if_false.accept(self)
            self.indent -= 1
        print("    " * self.indent + "}", end="")

    def visitPrinter(self, printer):
        print("print ", end="")
        printer.expr.accept(self)

    def visitReader(self, reader):
        print("read " + reader.name, end="")

    def visitFunctionCall(self, f_call):
        f_call.fun_expr.accept(self)
        print("(", end="")
        s_args = []
        for arg in f_call.args[:-1]:
            arg.accept(self)
            print(", ", end="")
        if f_call.args != []:
            f_call.args[-1].accept(self)
        print(")", end="")

    def visitReference(self, reference):
        print(reference.name, end="")

    def visitBinaryOperation(self, bin_op):
        print("(", end="")
        bin_op.lhs.accept(self)
        print(" " + bin_op.str_op + " ", end="")
        bin_op.rhs.accept(self)
        print(")", end="")

    def visitUnaryOperation(self, un_op):
        print("(", end="")
        print(un_op.str_op, end="")
        un_op.expr.accept(self)
        print(")", end="")


def test():
    pp = PrettyPrinter()
    pp.visit(Number(6))
    f_body = []
    bo = {"-(-2)": UnaryOperation("-", Number(-2))}
    bo["6 - (-(-2))"] = BinaryOperation(Number(6), "-", bo["-(-2)"])
    bo["6 - (-(-2)) + 7"] = BinaryOperation(bo["6 - (-(-2))"], "+", Number(7))
    f_body += [BinaryOperation(Number(1), "*", bo["6 - (-(-2)) + 7"])]
    f_body[0] = BinaryOperation(f_body[0], "%", Number(3))
    bo["1 >= -1"] = BinaryOperation(Number(1), ">=", Number(-1))
    bo["-(-3)"] = UnaryOperation("-", Number(-3))
    bo["3 == -(-3)"] = BinaryOperation(Number(3), "==", bo["-(-3)"])
    bo["3 == -(-3) > 0"] = BinaryOperation(bo["3 == -(-3)"], ">", Number(0))
    f_body += [UnaryOperation("!", bo["3 == -(-3) > 0"])]
    f_body[1] = BinaryOperation(bo["1 >= -1"], "*", f_body[1])
    bo["0 == 3"] = BinaryOperation(Number(0), "==", Number(3))
    f_body[1] = BinaryOperation(f_body[1], "-", bo["0 == 3"])
    bo["-4"] = UnaryOperation("-", Number(4))
    bo["(-4) + 2"] = BinaryOperation(bo["-4"], "+", Number(2))
    bo["(-4) + 2 + 3"] = BinaryOperation(bo["(-4) + 2"], "+", Number(3))
    foo1 = Function(["arg1", "arg2", "arg3", "arg4"],
                    [f_body[0],
                     f_body[1],
                     BinaryOperation(Number(11), "+", Number(-8)),
                     bo["(-4) + 2 + 3"]])
    foo2 = Function(["S", "D"], [BinaryOperation(Number(6), "+", Number(7)),
                                 UnaryOperation("!", Number(6))])
    pp.visit(FunctionDefinition("foo1", foo1))
    pp.visit(FunctionDefinition("foo2", foo2))
    function = Function([], [])
    definition = FunctionDefinition('x', function)
    uo = UnaryOperation("-", Reference('a'))
    call = FunctionCall(Reference('foo'), [bo["(-4) + 2 + 3"], uo])
    if_true = [Conditional(BinaryOperation(BinaryOperation(Number(42),
                                                           '+',
                                                           Reference('a')),
                                           '*',
                                           Number(0)),
                           [],
                           [Conditional(Number(42),
                                        [definition])])]
    conditional = Conditional(BinaryOperation(Number(42), '-', Number(42)),
                              if_true,
                              [call])
    pp.visit(definition)
    pp.visit(conditional)
    pp.visit(Read('x'))
    pp.visit(Print(BinaryOperation(Number(2), '+', Reference('a'))))
    pp.visit(call)
    boand = BinaryOperation(Number(1), "&&", Number(1))
    boor = BinaryOperation(Number(1), "||", Number(1))
    pp.visit(BinaryOperation(boand, "||", Number(1)))
    pp.visit(BinaryOperation(Number(1), "&&", boor))
    pp.visit(BinaryOperation(boor, "&&", Number(1)))
    pp.visit(BinaryOperation(Number(1), "||", boand))
    pp.visit(BinaryOperation(Number(1), "+", BinaryOperation(Number(2),
                                                             "*",
                                                             Number(3))))
    pp.visit(BinaryOperation(BinaryOperation(Number(1),
                                             "-",
                                             Number(2)),
                             "*",
                             Number(3)))
    pp.visit(BinaryOperation(Number(3), "/", BinaryOperation(Number(1),
                                                             "+",
                                                             Number(2))))
    pp.visit(BinaryOperation(BinaryOperation(Number(3),
                                             "/",
                                             Number(1)),
                             "-",
                             Number(2)))
    pp.visit(BinaryOperation(BinaryOperation(Number(1),
                                             "+",
                                             Number(2)),
                             "==",
                             Number(3)))
    pp.visit(BinaryOperation(Number(1), "+", BinaryOperation(Number(2),
                                                             "==",
                                                             Number(3))))
    pp.visit(BinaryOperation(Number(1), "!=", BinaryOperation(Number(2),
                                                              "-",
                                                              Number(3))))
    pp.visit(BinaryOperation(BinaryOperation(Number(1),
                                             "!=",
                                             Number(2)),
                             "*",
                             Number(3)))
    pp.visit(BinaryOperation(Number(1), ">", BinaryOperation(Number(2),
                                                             "<=",
                                                             Number(3))))
    pp.visit(BinaryOperation(BinaryOperation(Number(1),
                                             ">",
                                             Number(2)),
                             "<=",
                             Number(3)))
    pp.visit(BinaryOperation(Number(1), ">=", BinaryOperation(Number(3),
                                                              "/",
                                                              Number(2))))
    pp.visit(BinaryOperation(BinaryOperation(Number(1),
                                             "+",
                                             Number(-1)),
                             "&&",
                             Number(1)))
    pp.visit(BinaryOperation(Number(1), "+", BinaryOperation(Number(-1),
                                                             "&&",
                                                             Number(1))))
    pp.visit(BinaryOperation(Number(0), "||", BinaryOperation(Number(-1),
                                                              "*",
                                                              Number(1))))
    pp.visit(BinaryOperation(BinaryOperation(Number(2),
                                             "==",
                                             Number(3)),
                             "&&",
                             Number(4)))
    pp.visit(BinaryOperation(Number(2), "==", BinaryOperation(Number(3),
                                                              "&&",
                                                              Number(4))))
    pp.visit(BinaryOperation(BinaryOperation(Number(-1),
                                             "&&",
                                             Number(-2)),
                             "&&",
                             Number(-3)))


if __name__ == '__main__':
    test()
