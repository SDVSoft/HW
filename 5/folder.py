from yat.model import Scope, Number, ExpressionList, Function, FunctionCall
from yat.model import FunctionDefinition, Conditional, Print, Read, Reference
from yat.model import BinaryOperation, UnaryOperation
from yat.printer import PrettyPrinter


class ConstantFolder:
    def __init__(self):
        pass

    def visit(self, tree):
        return tree.accept(self)

    def visitNumber(self, num):
        return num

    def visitExpressionList(self, expr_list):
        expr_list = expr_list.expr_list
        new_list = []
        for expr in expr_list:
            new_list += [expr.accept(self)]
        return ExpressionList(new_list)

    def visitFunction(self, function):
        new_function = Function(function.args, [])
        new_function.body = function.body.accept(self)
        return new_function

    def visitFunctionDefinition(self, f_def):
        function = f_def.function
        new_f_def = FunctionDefinition(f_def.name, function.accept(self))
        return new_f_def

    def visitConditional(self, conditional):
        cond = Conditional(conditional.condition.accept(self), [])
        cond.if_true = conditional.if_true.accept(self)
        cond.if_false = conditional.if_false.accept(self)
        return cond

    def visitPrinter(self, printer):
        return Print(printer.expr.accept(self))

    def visitReader(self, reader):
        return reader

    def visitFunctionCall(self, f_call):
        new_fun_expr = f_call.fun_expr.accept(self)
        new_args = []
        for expr in f_call.args:
            new_args += [expr.accept(self)]
        return FunctionCall(new_fun_expr, new_args)

    def visitReference(self, reference):
        return reference

    def visitBinaryOperation(self, bin_op):
        lhs = bin_op.lhs.accept(self)
        rhs = bin_op.rhs.accept(self)
        result = BinaryOperation(lhs, bin_op.str_op, rhs)
        same_references = isinstance(lhs, Reference)
        same_references = same_references and isinstance(rhs, Reference)
        same_references = same_references and lhs.name == rhs.name
        reference_and_zero = isinstance(lhs, Reference)
        reference_and_zero = reference_and_zero and isinstance(rhs, Number)
        reference_and_zero = reference_and_zero and rhs == Number(0)
        zero_and_reference = isinstance(rhs, Reference)
        zero_and_reference = zero_and_reference and isinstance(lhs, Number)
        zero_and_reference = zero_and_reference and lhs == Number(0)
        if isinstance(lhs, Number) and isinstance(rhs, Number):
            return result.evaluate(Scope())
        elif same_references and bin_op.str_op == "-":
            return Number(0)
        elif reference_and_zero and bin_op.str_op == "*":
            return Number(0)
        elif zero_and_reference and bin_op.str_op == "*":
            return Number(0)
        else:
            return result

    def visitUnaryOperation(self, un_op):
        expr = un_op.expr.accept(self)
        result = UnaryOperation(un_op.str_op, expr)
        if isinstance(expr, Number):
            return result.evaluate(Scope())
        else:
            return result


def test():
    pp = PrettyPrinter()
    cf = ConstantFolder()
    pp.visit(cf.visit(Number(6)))
    f_body = []
    bo = {"-(-2)": UnaryOperation("-", Number(-2))}
    bo["6 - (-(-2))"] = BinaryOperation(Number(6), "-", bo["-(-2)"])
    bo["6 - (-(-2)) + 7"] = BinaryOperation(bo["6 - (-(-2))"], "+", Number(7))
    f_body += [BinaryOperation(Number(1), "*", bo["6 - (-(-2)) + 7"])]
    f_body[0] = BinaryOperation(f_body[0], "%", Number(3))
    bo["1 >= -1"] = BinaryOperation(Number(1), ">=", Number(-1))
    bo["-(-3)"] = UnaryOperation("-", Number(-3))
    bo["3 == -(-3)"] = BinaryOperation(Number(3), "==", bo["-(-3)"])
    bo["3 == -(-3) > 0"] = BinaryOperation(bo["3 == -(-3)"], ">", Number(0))
    f_body += [UnaryOperation("!", bo["3 == -(-3) > 0"])]
    f_body[1] = BinaryOperation(bo["1 >= -1"], "*", f_body[1])
    bo["0 == 3"] = BinaryOperation(Number(0), "==", Number(3))
    f_body[1] = BinaryOperation(f_body[1], "-", bo["0 == 3"])
    bo["-4"] = UnaryOperation("-", Number(4))
    bo["(-4) + 2"] = BinaryOperation(bo["-4"], "+", Number(2))
    bo["(-4) + 2 + 3"] = BinaryOperation(bo["(-4) + 2"], "+", Number(3))
    foo1 = Function(["arg1", "arg2", "arg3", "arg4"],
                    [f_body[0],
                     f_body[1],
                     BinaryOperation(Number(11), "+", Number(-8)),
                     bo["(-4) + 2 + 3"]])
    foo2 = Function(["S", "D"], [BinaryOperation(Number(6), "+", Number(7)),
                                 UnaryOperation("!", Number(6))])
    pp.visit(cf.visit(FunctionDefinition("foo1", foo1)))
    pp.visit(cf.visit(FunctionDefinition("foo2", foo2)))
    function = Function([], [])
    definition = FunctionDefinition('x', function)
    uo = UnaryOperation("-", Reference('a'))
    call = FunctionCall(Reference('foo'), [bo["(-4) + 2 + 3"], uo])
    if_true = [Conditional(BinaryOperation(BinaryOperation(Number(42),
                                                           '+',
                                                           Reference('a')),
                                           '*',
                                           Number(0)),
                           [],
                           [Conditional(Number(42),
                                        [definition])])]
    conditional = Conditional(BinaryOperation(Number(42), '-', Number(42)),
                              if_true,
                              [call])
    pp.visit(cf.visit(definition))
    pp.visit(cf.visit(conditional))
    pp.visit(cf.visit(Read('x')))
    pp.visit(cf.visit(Print(BinaryOperation(Number(2), '+', Reference('a')))))
    pp.visit(cf.visit(call))
    boand = BinaryOperation(Number(1), "&&", Number(1))
    boor = BinaryOperation(Number(1), "||", Number(1))
    pp.visit(cf.visit(BinaryOperation(boand, "||", Number(1))))
    pp.visit(cf.visit(BinaryOperation(Number(1), "&&", boor)))
    pp.visit(cf.visit(BinaryOperation(boor, "&&", Number(1))))
    pp.visit(cf.visit(BinaryOperation(Number(1), "||", boand)))
    pp.visit(cf.visit(BinaryOperation(Number(1), "+", Number(2))))
    pp.visit(cf.visit(BinaryOperation(Number(0), "*", Reference("f"))))
    pp.visit(cf.visit(BinaryOperation(Reference("h"), "*", Number(0))))
    pp.visit(cf.visit(BinaryOperation(Reference("name"),
                                      "-",
                                      Reference("name"))))


if __name__ == '__main__':
    test()
