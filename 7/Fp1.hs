head' (x:_) = x

tail' [] = []
tail' (_:xs) = xs

take' 0 _ = []
take' n (x:xs) = x:take' (n - 1) xs

drop' _ [] = []
drop' 0 xs = xs
drop' n (_:xs) = drop' (n - 1) xs

filter' _ [] = []
filter' f (x:xs) | f x       = x:filter' f xs
                 | otherwise = filter' f xs

foldl' _ z [] = z
foldl' f z (x:xs) = foldl' f (f z x) xs

concat' [] a = a
concat' (x:xs) a = x:concat' xs a

quickSort' [] = []
quickSort' (x:xs) = concat' (quickSort' (filter' (x >) xs)) (x:quickSort' (filter' (x <=) xs))