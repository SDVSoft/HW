#include <iostream>
#include "thread_pool.h"


Task::Task(void (*fun)(void *), void *arg)
    : arg(arg), done(false)
{
    f = fun;
    pthread_mutex_init(&task_mutex, NULL);
    pthread_cond_init(&task_cond, NULL);
}

Task::Task()
    : f(NULL), arg(NULL), done(false)
{
    pthread_mutex_init(&task_mutex, NULL);
    pthread_cond_init(&task_cond, NULL);
}

Task::~Task() {
    pthread_mutex_destroy(&task_mutex);
    pthread_cond_destroy(&task_cond);
}

void Task::evaluate() {
    pthread_mutex_lock(&task_mutex);
    f(arg);
    done = true;
    pthread_cond_broadcast(&task_cond);
    pthread_mutex_unlock(&task_mutex);
    return;
}

void Task::wait() {
    pthread_mutex_lock(&task_mutex);
    while (!done)
        pthread_cond_wait(&task_cond, &task_mutex);
    pthread_mutex_unlock(&task_mutex);
    return;
}

void* performer(void *arg) {
    ThreadPool *pool = (ThreadPool *)arg;
    int working = 0;
    Task *t = NULL;
    while (1) {
        pthread_mutex_lock(&pool->pool_mutex);
        if (!pool->tasks.empty()) {
            t = pool->tasks.front();
            pool->tasks.pop();
            if (!working) {
                working = 1;
                pool->active++;
            }
        }
        else {
            if (working) {
                working = 0;
                pool->active--;
                pthread_cond_broadcast(&pool->pool_cond);
            }
            if (pool->active == 0 && pool->end) {
                pthread_mutex_unlock(&pool->pool_mutex);
                break;
            }
            pthread_cond_wait(&pool->pool_cond, &pool->pool_mutex);
        }
        pthread_mutex_unlock(&pool->pool_mutex);
        if (working) {
            t->evaluate();
        }
    }
    return NULL;
}

ThreadPool::ThreadPool(int size)
    : size(size), active(0), end(false), threads(new pthread_t[size])
{
    pthread_mutex_init(&pool_mutex, NULL);
    pthread_cond_init(&pool_cond, NULL);
    for (int i = 0; i < size; i++)
        pthread_create(&threads[i], NULL, performer, this);
}

ThreadPool::~ThreadPool() {
    void *retval;
    pthread_mutex_lock(&pool_mutex);
    end = true;
    pthread_cond_broadcast(&pool_cond);
    pthread_mutex_unlock(&pool_mutex);
    for (int i = 0; i < size; i++)
        pthread_join(threads[i], &retval);
    delete[] threads;
    pthread_mutex_destroy(&pool_mutex);
    pthread_cond_destroy(&pool_cond);
}

void ThreadPool::submit(Task *task) {
    pthread_mutex_lock(&pool_mutex);
    tasks.push(task);
    pthread_cond_signal(&pool_cond);
    pthread_mutex_unlock(&pool_mutex);
    return;
}
