#include <string>
#include <algorithm>
#include <iostream>
#include <list>
#include "thread_pool.h"

pthread_mutex_t sort_mutex;
pthread_cond_t sort_cond;
int sorting_tasks;
std::list<Task> sort_task_list;

typedef struct sort_args {
    int *data;
    int size;
    ThreadPool *pool;
    int depth;
    int sort_tasks;
} sort_args;

int partition(int *data, int size, int pivot) {
    int left = 0, right = size - 1;
    while (left <= right) {
        if (data[left] > pivot && data[right] < pivot)
            std::swap(data[left++], data[right--]);
        else {
            if (data[left] <= pivot) left++;
            if (data[right] >= pivot) right--;
        }
    }
    return left;
}

void pqsort(void *args) {
    int size = ((sort_args *)args)->size, depth = ((sort_args *)args)->depth;
    if (size <= 1) {
        free(args);
        pthread_mutex_lock(&sort_mutex);
        sorting_tasks--;
        pthread_cond_signal(&sort_cond);
        pthread_mutex_unlock(&sort_mutex);
        return;
    }
    int *data = ((sort_args *)args)->data;
    ThreadPool *pool = ((sort_args *)args)->pool; 
    free(args);
    if (depth == 0) {
        std::sort(&data[0], &data[size]);
        pthread_mutex_lock(&sort_mutex);
        sorting_tasks--;
        pthread_cond_signal(&sort_cond);
        pthread_mutex_unlock(&sort_mutex);
        return;
    }
    int lsize = partition(data, size, data[size / 2]), rsize = size - lsize;
    sort_args *argsl = (sort_args *) malloc(sizeof(sort_args)), *argsr = (sort_args *) malloc(sizeof(sort_args));
    argsl->data = data, argsr->data = data + lsize;
    argsl->size = lsize, argsr->size = rsize;
    argsl->pool = pool, argsr->pool = pool;
    argsl->depth = depth - 1, argsr->depth = depth - 1;
    pthread_mutex_lock(&sort_mutex);
    sort_task_list.push_back(Task());
    sort_task_list.back() = Task(pqsort, (void *) argsl);
    Task *taskl = &sort_task_list.back();
    sort_task_list.push_back(Task());
    sort_task_list.back() = Task(pqsort, (void *) argsr);
    Task *taskr = &sort_task_list.back();
    sorting_tasks++;
    pthread_mutex_unlock(&sort_mutex);
    pool->submit(taskl);
    pool->submit(taskr);
    return;
}

int main(int argc, char *argv[]) {
    if (argc == 4) {
        char *end;
        ThreadPool pool = ThreadPool(strtol(argv[1], &end, 10));
        int size = strtol(argv[2], &end, 10);
        int arr[size];
        srand(42);
        for (int i = 0; i < size; i++)
            arr[i] = rand();
        sort_args *args = (sort_args *) malloc(sizeof(sort_args));
        args->data = arr;
        args->size = size;
        args->pool = &pool;
        args->depth = strtol(argv[3], &end, 10);
        pthread_mutex_init(&sort_mutex, NULL);
        pthread_cond_init(&sort_cond, NULL);
        sorting_tasks = 1;
        sort_task_list.push_back(Task());
        sort_task_list.back() = Task(pqsort, (void *) args);
        pool.submit(&sort_task_list.back());
        while (1) {
            pthread_mutex_lock(&sort_mutex);
            if (sorting_tasks)
                pthread_cond_wait(&sort_cond, &sort_mutex);
            else {
                pthread_mutex_unlock(&sort_mutex);
                break;
            }
            pthread_mutex_unlock(&sort_mutex);
        }
        pthread_mutex_destroy(&sort_mutex);
        pthread_cond_destroy(&sort_cond);
        for (int i = 0; i < size; i++)
            std::cout << arr[i] << std::endl;
    }
    return 0;
}
