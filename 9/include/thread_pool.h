#pragma once

#include <pthread.h>
#include <queue>
#include <list>


class Task {
  public:
    Task(void (*f)(void *), void *arg);
    Task();
    ~Task();
    void evaluate();
    void wait();
    void (*f)(void *);
    void *arg;
    bool done;
    pthread_mutex_t task_mutex;
    pthread_cond_t task_cond;
};

struct ThreadPool {
    ThreadPool(int size);
    ~ThreadPool();
    void submit(Task *task);
    int size;
    int active;
    bool end;
    pthread_mutex_t pool_mutex;
    pthread_cond_t pool_cond;
    pthread_t *threads;
    std::queue<Task*> tasks;
};
