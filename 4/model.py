import operator


class Scope:
    def __init__(self, parent=None):
        self.values = {}
        self.parent = parent

    def __getitem__(self, key):
        if key in self.values:
            return self.values[key]
        elif self.parent:
            return self.parent[key]
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        self.values[key] = value


class Number:
    def __init__(self, value):
        self.value = value

    def evaluate(self, scope):
        return self

    def __eq__(self, number):
        return self.value == number.value

    def __hash__(self):
        return hash(self.value)

    def __bool__(self):
        return bool(self.value)


class ExpressionList:
    def __init__(self, expr_list):
        self.expr_list = expr_list or []

    def evaluate(self, scope):
        result = None
        for expr in self.expr_list:
            result = expr.evaluate(scope)
        return result


class Function:
    def __init__(self, args, body):
        self.args = args
        self.body = ExpressionList(body)

    def evaluate(self, scope):
        return self


class FunctionDefinition:
    def __init__(self, name, function):
        self.name = name
        self.function = function

    def evaluate(self, scope):
        scope[self.name] = self.function
        return self.function


class Conditional:
    def __init__(self, condition, if_true, if_false=None):
        self.condition = condition
        self.if_true = ExpressionList(if_true)
        self.if_false = ExpressionList(if_false)

    def evaluate(self, scope):
        if self.condition.evaluate(scope):
            return self.if_true.evaluate(scope)
        else:
            return self.if_false.evaluate(scope)


class Print:
    def __init__(self, expr):
        self.expr = expr

    def evaluate(self, scope):
        result = self.expr.evaluate(scope)
        print(result.value)
        return result


class Read:
    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        scope[self.name] = Number(int(input()))
        return scope[self.name]


class FunctionCall:
    def __init__(self, fun_expr, args):
        self.fun_expr = fun_expr
        self.args = args

    def evaluate(self, scope):
        function = self.fun_expr.evaluate(scope)
        call_scope = Scope(scope)
        for name, expr in zip(function.args, self.args):
            call_scope[name] = expr.evaluate(scope)
        return function.body.evaluate(call_scope)


class Reference:
    def __init__(self, name):
        self.name = name

    def evaluate(self, scope):
        return scope[self.name]


class OperationError(ValueError):
    '''Raise when unsupported or unrecognized operation is
       given as an argument to the object of Operation class'''


class BinaryOperation:
    OPERATION = {"+": operator.add,
                 "-": operator.sub,
                 "*": operator.mul,
                 "/": operator.floordiv,
                 "%": operator.mod,
                 "==": operator.eq,
                 "!=": operator.ne,
                 "<": operator.lt,
                 ">": operator.gt,
                 "<=": operator.le,
                 ">=": operator.ge,
                 "&&": lambda lhs, rhs: lhs and rhs,
                 "||": lambda lhs, rhs: lhs or rhs}

    def __init__(self, lhs, op, rhs):
        self.lhs = lhs
        self.rhs = rhs
        if op in self.OPERATION:
            self.op = self.OPERATION[op]
        else:
            raise OperationError("unsupported binary operation: " + op)

    def evaluate(self, scope):
        lhs_result = self.lhs.evaluate(scope).value
        rhs_result = self.rhs.evaluate(scope).value
        return Number(int(self.op(lhs_result, rhs_result)))


class UnaryOperation:
    OPERATION = {"!": lambda expr: int(not expr),
                 "-": lambda expr: -expr}

    def __init__(self, op, expr):
        self.expr = expr
        if op in self.OPERATION:
            self.op = self.OPERATION[op]
        else:
            raise OperationError("unsupported unary operation: " + op)

    def evaluate(self, scope):
        result = self.expr.evaluate(scope).value
        return Number(self.op(result))


def example():
    parent = Scope()
    parent["foo"] = Function(('hello', 'world'),
                             [Print(BinaryOperation(Reference('hello'),
                                                    '+',
                                                    Reference('world')))])
    parent["bar"] = Number(10)
    scope = Scope(parent)
    assert 10 == scope["bar"].value
    scope["bar"] = Number(20)
    assert scope["bar"].value == 20
    print('It should print 2: ', end=' ')
    FunctionCall(FunctionDefinition('foo', parent['foo']),
                 [Number(5), UnaryOperation('-',
                                            Number(3))]).evaluate(scope)


def tests():
    print("Tests' results:")
    errors, no_errors_at_all = False, True
    num = {i - 20: Number(i - 20) for i in range(51)}
    num1 = Number(1)
    parent = Scope()
    if num1 != num[1] or num[-1] == num[1]:
        errors = True
    if num[0] == num[10] or num[1].value != 1:
        errors = True
    if num[4].evaluate(parent) != num[4]:
        errors = True
    if num[-7].evaluate(parent) != num[-7]:
        errors = True
    d = {}
    d[num[0]] = 0
    d[num[-5]] = 5
    d[num[4]] = -4
    if len(d) != 3 or d[num[0]] != 0 or d[Number(4)] != -4:
        errors = True
    d[num[4]] = num[-4]
    if len(d) != 3 or d[num[4]] != num[-4]:
        errors = True
    if not errors:
        print("No errors in class 'Number'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Number'")
    errors = False

    parent["0"] = 0
    parent["str1"] = 1
    if len(parent.values) != 2 or parent["str1"] != 1:
        errors = True
    parent["str1"] = 2
    if len(parent.values) != 2 or parent["str1"] != 2:
        errors = True
    scope = Scope(parent)
    scope["0"] = num[10]
    if scope["0"] != num[10] or scope["str1"] != 2:
        errors = True
    if not errors:
        print("No errors in class 'Scope'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Scope'")
    errors = False

    if UnaryOperation('-', num[-10]).evaluate(scope) != num[10]:
        errors = True
    if UnaryOperation('-', num[0]).evaluate(scope) != num[0]:
        errors = True
    if UnaryOperation('-', num[1]).evaluate(scope) != num[-1]:
        errors = True
    if UnaryOperation('!', num[1]).evaluate(scope) != num[0]:
        errors = True
    if UnaryOperation('!', num[0]).evaluate(scope) == num[0]:
        errors = True
    if UnaryOperation('!', num[-10]).evaluate(scope) != num[0]:
        errors = True
    unary_operation_1 = UnaryOperation('!', num[0])
    if UnaryOperation('!', unary_operation_1).evaluate(scope) != num[0]:
        errors = True
    unary_operation_2 = UnaryOperation('!', num[1])
    if UnaryOperation('!', unary_operation_2).evaluate(scope) == num[0]:
        errors = True
    unary_operation_3 = UnaryOperation('-', num[4])
    if UnaryOperation('-', unary_operation_3).evaluate(scope) != num[4]:
        errors = True
    unary_operation_4 = UnaryOperation('-', unary_operation_3)
    if UnaryOperation('-', unary_operation_4).evaluate(scope) != num[-4]:
        errors = True
    if not errors:
        print("No errors in class 'UnaryOperation'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'UnaryOperation'")
    errors = False

    if BinaryOperation(num[1], "+", num[20]).evaluate(scope) != num[21]:
        errors = True
    if BinaryOperation(num[0], "+", num[10]).evaluate(scope) != num[10]:
        errors = True
    if BinaryOperation(num[-10], "+", num[1]).evaluate(scope) != num[-9]:
        errors = True
    if BinaryOperation(num[11], "-", num[20]).evaluate(scope) != num[-9]:
        errors = True
    if BinaryOperation(num[2], "-", num[0]).evaluate(scope) != num[2]:
        errors = True
    if BinaryOperation(num[20], "-", num[-1]).evaluate(scope) != num[21]:
        errors = True
    if BinaryOperation(num[5], "*", num[6]).evaluate(scope) != num[30]:
        errors = True
    if BinaryOperation(num[0], "*", num[11]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-9], "*", num[2]).evaluate(scope) != num[-18]:
        errors = True
    if BinaryOperation(num[-1], "*", num[-2]).evaluate(scope) != num[2]:
        errors = True
    if BinaryOperation(num[20], "/", num[5]).evaluate(scope) != num[4]:
        errors = True
    if BinaryOperation(num[0], "/", num[-5]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-10], "/", num[-10]).evaluate(scope) != num[1]:
        errors = True
    if BinaryOperation(num[15], "/", num[-3]).evaluate(scope) != num[-5]:
        errors = True
    if BinaryOperation(num[19], "%", num[4]).evaluate(scope) != num[3]:
        errors = True
    if BinaryOperation(num[15], "%", num[5]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[0], "%", num[4]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-10], "%", num[6]).evaluate(scope) != num[2]:
        errors = True
    if BinaryOperation(num[10], "==", num[10]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[19], "==", Number(19)).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[-15], "==", Number(-15)).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[29], "!=", num[4]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[-9], "!=", num[9]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[19], "<", num[4]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-10], "<", num[1]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[0], "<", num[0]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[0], ">", num[4]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[19], ">", num[4]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[-1], ">", num[-11]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[14], "<=", Number(14)).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[-5], "<=", num[3]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[0], "<=", num[-11]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-4], ">=", num[-11]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[21], ">=", num[20]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[4], ">=", num[5]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[0], "&&", num[0]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[0], "&&", num[-11]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-1], "&&", num[0]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[10], "&&", num[-1]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[0], "||", num[0]).evaluate(scope) != num[0]:
        errors = True
    if BinaryOperation(num[-1], "||", num[1]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[0], "||", num[16]).evaluate(scope) == num[0]:
        errors = True
    if BinaryOperation(num[4], "||", num[0]).evaluate(scope) == num[0]:
        errors = True
    bin_operation_1 = BinaryOperation(BinaryOperation(num[1], "+", num[7]),
                                      "/",
                                      num[-2])
    bin_operation_2 = BinaryOperation(num[-6], "*", bin_operation_1)
    if bin_operation_1.evaluate(scope) != num[-4]:
        errors = True
    if bin_operation_2.evaluate(scope) != num[24]:
        errors = True
    result = BinaryOperation(bin_operation_1,
                             "+",
                             bin_operation_2).evaluate(scope)
    if num[20] != result:
        errors = True
    bin_operation_3 = BinaryOperation(num[6], ">=", num[7])
    bin_operation_4 = BinaryOperation(num[1], "&&", bin_operation_3)
    if bin_operation_4.evaluate(scope) != num[0]:
        errors = True
    if not errors:
        print("No errors in class 'BinaryOperation'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'BinaryOperation'")
    errors = False

    foo1 = Function(["arg1", "arg2", "arg3", "arg4"],
                    [num[1], num[2], BinaryOperation(num[11], "+", num[-8]),
                     UnaryOperation("-", num[4])])
    if foo1.evaluate(scope).args != ["arg1", "arg2", "arg3", "arg4"]:
        errors = True
    if foo1.evaluate(scope).body.evaluate(scope) != num[-4]:
        errors = True
    if not errors:
        print("No errors in class 'Function'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Function'")
    errors = False

    scope = Scope()
    function_name = "function"
    foo2 = FunctionDefinition(function_name, foo1).evaluate(scope)
    if foo2.args != scope[function_name].args:
        errors = True
    foo_from_scope = scope[function_name]
    if foo1.body.evaluate(scope) != foo_from_scope.body.evaluate(scope):
        errors = True
    if not errors:
        print("No errors in class 'FunctionDefinition'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'FunctionDefinition'")
    errors = False

    expr = {0: BinaryOperation(num[1], "&&", num[1]),
            1: BinaryOperation(num[0], "&&", num[6]),
            2: BinaryOperation(num[2], "||", num[1]),
            3: BinaryOperation(num[-1], ">", num[1]),
            4: BinaryOperation(num[19], "==", num[19]),
            5: BinaryOperation(num[1],
                               "&&",
                               BinaryOperation(num[1], "<=", num[0])),
            6: BinaryOperation(num[6], "+", num[12]),
            7: BinaryOperation(num[10], "-", num[21]),
            8: BinaryOperation(num[8], "*", num[3]),
            9: BinaryOperation(num[16], "/", num[-4]),
            10: BinaryOperation(num[7],
                                "%",
                                BinaryOperation(num[15], "/", num[3])),
            11: BinaryOperation(num[-8], "-", num[-8])}
    if Conditional(expr[0], [expr[6]]).evaluate(scope) != num[18]:  # True
        errors = True
    if Conditional(expr[1],
                   [expr[6]],
                   [expr[7]]).evaluate(scope) != num[-11]:  # False
        errors = True
    if Conditional(expr[2],
                   [expr[8]],
                   [expr[9]]).evaluate(scope) != num[24]:  # True
        errors = True
    if Conditional(expr[9],
                   [expr[10]],
                   [expr[6]]).evaluate(scope) != num[2]:  # True
        errors = True
    if Conditional(expr[5],
                   [expr[6]],
                   [expr[1], expr[4],
                    expr[8], expr[9]]).evaluate(scope) != num[-4]:  # False
        errors = True
    if Conditional(Conditional(expr[5], [expr[6]], [expr[9]]),
                   [expr[11]]).evaluate(scope) != num[0]:  # True
        errors = True
    conditional_1 = Conditional(expr[4], [expr[6]])  # True
    conditional_2 = Conditional(expr[1], [expr[7]], [expr[8]])  # False
    conditional_result = Conditional(num[0],
                                     [conditional_1],
                                     [conditional_2]).evaluate(scope)
    if conditional_result != expr[8].evaluate(scope):
        errors = True
    if Conditional(expr[3], [expr[11]]).evaluate(scope):  # False
        errors = True
    if Conditional(num[1], None).evaluate(scope):
        errors = True
    if Conditional(num[0], [], []).evaluate(scope):
        errors = True
    if not errors:
        print("No errors in class 'Conditional'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Conditional'")
    errors = False

    scope = Scope()
    foo1 = Function(["arg1", "arg2", "arg3", "arg4"],
                    [num[1], num[2], BinaryOperation(num[11], "+", num[-8]),
                     UnaryOperation("-", num[4])])
    f_call = FunctionCall(foo1, [num[10],
                                 UnaryOperation("-", num[6]),
                                 BinaryOperation(num[7], "*", num[4]),
                                 Number(99)])
    if f_call.evaluate(scope) != num[-4]:
        errors = True
    foo2 = Function(["S", "D"], [BinaryOperation(num[6], "+", num[7]),
                                 UnaryOperation("!", num[6])])
    f_call_args = [num[7], num[13], num[13],
                   BinaryOperation(num[1], "-", num[1]),
                   Conditional(UnaryOperation("!", num[1]), None,
                               [BinaryOperation(num[9], "*", num[3])])]
    if FunctionCall(Conditional(num[0], [num[1]], [foo2]),
                    f_call_args).evaluate(scope) != num[0]:
        errors = True
    if not errors:
        print("No errors in class 'FunctionCall'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'FunctionCall'")
    errors = False

    parent = Scope()
    parent["Number(6)"] = num[6]
    foo1 = Function(["arg1", "arg2", "arg3", "arg4"],
                    [num[1], num[2], BinaryOperation(num[11], "+", num[-8]),
                     UnaryOperation("-", num[4])])
    parent["foo1"] = foo1
    scope = Scope(parent)
    foo2 = foo2 = Function(["S", "D"], [BinaryOperation(num[6], "+", num[7]),
                                        UnaryOperation("!", num[6])])
    scope["foo2"] = foo2
    if Reference("Number(6)").evaluate(parent) != num[6]:
        errors = True
    if Reference("Number(6)").evaluate(scope) != num[6]:
        errors = True
    if Reference("foo1").evaluate(parent).args != foo1.args:
        errors = True
    if Reference("foo1").evaluate(scope).args != foo1.args:
        errors = True
    if Reference("foo2").evaluate(scope).args != foo2.args:
        errors = True
    if Reference("Number(6)").evaluate(parent) != num[6]:
        errors = True
    if not errors:
        print("No errors in class 'Reference'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Reference'")
    errors = False

    scope = Scope()
    print("Enter '3':")
    num3 = Read("3").evaluate(scope)
    print("Enter '-1':")
    num1 = UnaryOperation("-", Read("-1").evaluate(scope)).evaluate(scope)
    print("Enter '22':")
    num22 = Read("twenty two").evaluate(scope)
    if len(scope.values) != 3 or scope["3"] != num[3]:
        errors = True
    if num22 != scope["twenty two"] or num1 != num[1]:
        errors = True
    print("Enter '0':")
    num0 = Read("3").evaluate(scope)
    if len(scope.values) != 3 or scope["3"] != num[0]:
        errors = True
    if not errors:
        print("No errors in class 'Read'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Read' OR INCORRECT INPUT")
    errors = False

    print("It should be 8: ", end=" ")
    num8 = Print(num[8]).evaluate(scope)
    if num8 != num[8]:
        errors = True
    print("It should be -11: ", end=" ")
    num11 = Print(BinaryOperation(BinaryOperation(num[10], "*", num[22]),
                                  "/",
                                  num[-20])).evaluate(scope)
    if num11 != num[-11]:
        errors = True
    print("It should be 0: ", end=" ")
    num0 = Print(Conditional(num[1],
                 [BinaryOperation(num[17], "&&", num[0])])).evaluate(scope)
    if num0 != num[0]:
        errors = True
    print("It should be 0: ", end=" ")
    num0 = Print(BinaryOperation(num[17], "<=", num[0])).evaluate(scope)
    if num0 != num[0]:
        errors = True
    if not errors:
        print("No errors in class 'Print'")
    else:
        no_errors_at_all = False
        print("ERRORS IN CLASS 'Print'")
    errors = False

    if no_errors_at_all:
        print("Tests have been successfully completed without any errors?")
    else:
        print("Tests have been completed with errors!")


if __name__ == '__main__':
    example()
    tests()
